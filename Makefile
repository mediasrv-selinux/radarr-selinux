include /usr/share/selinux/devel/Makefile

define assign-radarr-ports
echo "Assigning radarr port..."
semanage port -m -t radarr_port_t -p tcp 7878
endef

define remove-radarr-ports
echo "Removing radarr port..."
semanage port -d -t radarr_port_t -p tcp 787
endef

define relabel-radarr-files
echo "Relabeling files..."
restorecon -DR /var/lib/radarr/
restorecon -DR /opt/radarr/
endef

define force-relabel-radarr-files
echo "Relabeling files..."
restorecon -R /var/lib/radarr/
restorecon -R /opt/radarr/
endef

.PHONY: install uninstall update

install:
	semodule -v -i radarr.pp
	$(assign-radarr-ports)
	$(relabel-radarr-files)

uninstall:
	semodule -v -r radarr
	$(remove-radarr-ports)
	$(relabel-radarr-files)	

update:
	semodule -v -i radarr.pp
	$(force-relabel-radarr-files)

