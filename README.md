# radarr-selinux

This is an SELinux policy module that defines a domain to confine radarr
and associated processes. By default, radarr runs in the
`unconfined_service_t` domain. This gives radarr far more access to the system
than it needs to do its job. Installing this policy module will confine radarr
in its own domain with a tailored set of access controls.

This policy module should not interfere with radarr's normal operation except
where noted (i.e Web UI file browser). If you find something is not working
correctly please open an issue. However all booleans default to **off** and
you will need to enable the ones matching your chosen download clients and search
clients or radarr will not be able to function.

The policy is currently designed to work with the [plex](https://gitlab.com/mediasrv-selinux/plex-selinux),
[NZBget](https://gitlab.com/mediasrv-selinux/nzbget-selinux), [jackett](https://gitlab.com/mediasrv-selinux/jackett-selinux) and [transmission](https://gitlab.com/mediasrv-selinux/transmission-selinux) policies. It is modular using conditional statements within the policy so it has
no dependencies on any other module. Additional download clients and search clients may be added easily.

Bug reports and patches are welcome.

### Support Status

Supported Platforms: CentOS 7-8, Fedora 29-32

Supported radarr Version: 0.2+

### Current Release Notes

The included radarr.service file ensures that radarr is run under the proper SELinux
domain. You may use it as is or change it to suit your system.

Automatic update (self update) is not supported when using this policy, if SELinux is
enforcing it *will* prevent updates. Using the RPM package or manually updating is required.

The policy requires that radarr's home directory be `/var/lib/radarr` instead
of the traditional location in /home. This is in line with Red Hats file system
standards. You may simply edit the user and move /home/radarr/.config to the new location.
Installing the module will re-label the files correctly.

## Building

### Requirements

In order to build and install this policy module you will need the following
packages installed:

* make
* selinux-policy
* selinux-policy-devel
* policycoreutils-python for Red Hat/CentOS <= 7 and Fedora <=31
* python3-policycoreutils for Red Hat CentOS 8+ and Fedora 32+


### Build

	$ make

### Install

Installs the policy package and relabels radarr's files to new file
contexts.

(as root)

	# make install

### Update

Updates the policy without modifying the SElinux port contexts to save time.

    # make update

### Uninstall

Uninstalls the policy module from the system and relabels radarr's
files to the default file contexts.

(as root)

	# make uninstall

## Policy Details

### Domain

SELinux uses domains to label processes. The domain of a confined process
defines what access controls the process is subjected to.

The security context of a process can be inspected by adding the `-Z` option to
the `ps` command

	$ ps -eZ | grep radarr_t
	system_u:system_r:radarr_t:s0    474378 ?      00:02:57 mono

This policy module defines the domain `radarr_t` for radarr processes.

### File Contexts

SELinux requires that files be labeled with a security context so it knows what
access controls to apply. This security context is usually stored in an extended
attribute on the file.

The security context of a file can be inspected by using the `-Z` option with
the `ls` command.

	$ ls -Z /opt/radarr
	system_u:object_r:radarr_exec_t:s0 radarr.exe

A file's security context can be changed temporarily using `chcon`. These
changes will not survive a relabeling.

	$ chcon -R /usr/share/my_media_library

A file's security context can be changed permanently by using `semanage fcontext`
to define the default security context for a given set of files and then using
`restorecon` to apply those labels.

	$ semanage fcontext -a -t radarr_var_lib_t "/opt/radarr(/.*)?"
	$ restorecon -v -R /opt/radarr

This policy module defines the following file contexts:

##### radarr\_etc\_t

radarr configuration files.
<br />
<br />

##### radarr\_initrc\_exec\_t

radarr init.d scripts or systemd unit files.
<br />
<br />

##### radarr\_exec\_t

radarr executables. Files labeled with this context will create
processes in the `radarr_t` domain when called from certain domains
(notably `init_t` and `initrc_t`). Generally a transition to `radarr_t` will not
occur if the calling process is `unconfined_t` (the standard user context in
the targeted policy). This means if a user calls the executable from a shell
the resulting process **will not** be confined.
<br />
<br />

##### radarr\_log\_t

radarr log files. Using this context will allow common logging utils such
as logrotate to access and manipulate these files. Processes confined by `radarr_t`
can manage files and directories in this domain.
<br />
<br />

##### radarr\_var\_lib\_t

radarr `/var/lib` files. Processes confined by `radarr_t` can manage
files and directories in this domain.
<br />
<br />

##### radarr\_tmp\_t and radarr\_tmpfs\_t

radarr temporary files and objects. Processes confined by `radarr_t`
can manage files and directories in this domain.
<br />
<br />

##### Executable contexts

Processes confined by `radarr_t` have permission to execute files in the
following security contexts:

* `radarr_exec_t`
* `bin_t`

Processes created from executables in these contexts will be confined by `radarr_t`.
<br />
<br />

##### Shared contexts

Processes confined by `radarr_t` have read access to the following
shared file contexts:

* `public_content_t`
* `public_content_rw_t`

This is useful if you want label media files to be readable by processes in
domains other than `radarr_t` (Apache, NFS, Samba, FTP, etc)
<br />
<br />

### Networking

Processes confined by `radarr_t` have the following network capabilities:

* May bind TCP sockets to `radarr_port_t`
* May connect TCP sockets to `http_port_t`
* May connect TCP sockets to `port_t`
* May make DNS queries (TCP and UDP).
* May send log messages to syslog/journald.

### Booleans

SELinux booleans can be used to adjust the access controls enforced for
`radarr_t`. Default settings (off) result in tightest security. Enabling a boolean
will loosen access controls and allow `radarr_t` confined processes more access
to the system.

The status of booleans can be inspected using `getsebool`

	$ getsebool -a | grep radarr
	radarr_access_all_ro --> off
	radarr_access_all_rw --> off
	radarr_access_home_dirs_rw --> off
	radarr_access_sys_cert_ro --> off
	radarr_anon_write --> off
	radarr_mediasrv_write --> on
	radarr_send_email --> off

A boolean can be turned on or off using `setsebool`

(as root)

`# setsebool allow_radarr_list_all_dirs on`

A `-P` option can be passed to `setsebool` that makes the setting persist
across reboots.

This policy modules defines the following booleans:

##### radarr\_access\_all\_ro

Allow processes contained by `radarr_t` to read all files and directories.
<br />
<br />
Defaults to **off**. Should not be needed or used.
<br />
<br />

##### radarr\_access\_all\_rw

Allow processes contained by `radarr_t` to manage (read, write, create, delete)
all files and directories.
<br />
<br />
Defaults to **off** and should remain off. If you need this you are doing it wrong!
<br />
<br />

##### radarr\_anon\_write

Allow processes contained by `radarr_t` to manage (read, write, create, delete)
files and directories labeled with the `public_content_rw_t` context.
<br />
<br />
Defaults to **off**
<br />
<br />

##### radarr\_access\_home\_dirs\_rw

Allow processes contained by `radarr_t` to manage (read, write, create, delete)
files and directories in user home directories.
<br />
<br />
Defaults to **off**
<br />
<br />

##### radarr\_sys\_certs\_ro

Allows processes contained by `radarr_t` to read cert\_t labeled files, normally this
is the system wide TLS certificates stored in /etc/pki/
<br />
<br />
This defaults to **on** so that the SSL certs for radarr may be stored in the default locations.
<br />
<br />

##### radarr\_send\_email

Allows processes contained by `radarr_t` to connect to `smtp_port_t` to send email notifications.
<br />
<br />
This defaults to **off**.
<br />
<br />

### Optional Booleans

These values are only present if the applications and selinux polices from them are installed
on the system.

##### radarr\_use\_mediasrv

Allows processes contained by `radarr_t` to use the Mediaserver Framework shared contexts. This
allows for a shared file context between multiple media downloading and sharing applications
without using the `public_content_rw_t` context.
<br />
<br />
This defaults to **off**.

If enabled `radarr_t` will have access to the following shared contexts:

* `mediasrv_content_ro_t`
* `mediasrv_content_rw_t`
<br />
<br />

##### radarr\_connect\_jackett

Allows processes contained by `radarr_t` to connect to `jackett_port_t` and use the
jackett indexing/search service.
<br />
<br />
This defaults to **off**.
<br />
<br />

##### radarr\_connect\_nzbget

Allows processes contained by `radarr_t` to connect to `nzbget_port_t` and use the
nzbget unsenet download service.
<br />
<br />
This defaults to **off**.

If enabled `radarr_t` will have access to the following shared context:

* `nzbget_content_rw_t`
<br />
<br />

##### radarr\_connect\_transmission

Allows processes contained by `radarr_t` to connect to `transmissiont_port_t` and use the
transmission bittorrent download service.
<br />
<br />
This defaults to **off**.

If enabled `radarr_t` will have access to the following shared context:

* `transmission_content_rw_t`
<br />
<br />





